## Things you should know ##

Without attempting to provide a comprehensive tutorial, there are few
more important tasks you will perform once in a while.  Here they are.

### The remote repository has changed while making changes locally ###

The typical situation is that a `git pull` fails because you forgot to
commit your local changes beforehand.  Or you are notified of new
commits to the remote repository.  You have three options.

* Your work is not done, but you would like to integrate the changes
  on the remote before continuing:  Type

         git stash

    to "stash away" your changes, followed by 

         git pull
         git stash pop

    This pulls the changes from the remote, then restores your changes
    _on top_ of the current state of the remote.  It is possible, of
    course, that your changes and the remote changes are in conflict and
    need to be resolved, see _merge conflicts_ below.

* Your work is done, or you want to finish your work before looking at
  the remote: Commit your finished work as usual, then `git pull`,
  resolve merge conflicts if any, see below, and `git push` the merged
  version to the remote.

* You cannot deal with the merge yourself: switch to your own branch,
  commit your changes, push _your_ branch to the server, and notify
  one of your collaborators to sort it all out.  See _Working with
  branches_ below.

Good to know: you can always probe the remote server by typing `git
fetch` or `git remote update` (the latter if you have several remote
branches) followed by `git status`.  These commands do not change your
local branch, but the screen output will tell you whether your local
version has diverged from the remote.

### Merge conflicts ###

When you `git pull` or `git merge` or `git stash pop`, two things can
happen:

1. The automatic merge proceeds cleanly.  Then you can go ahead
    straight away.

2.  There is a merge conflict.  Git will tell you which files are
    affected.  It will put both conflicting versions side by side into
    each file, so you can look at the conflicts and edit them any way
    you like.  _Finish this process with an explicit commit as
    described above_.  You may simply commit an unclean version that
    still contains alternative versions side-by-side and ask a
    collaborator to sort it all out.  _However, if you don't commit at
    all at this point, you are stuck!_

You will find that a automatic merge proceeds cleanly surprisingly
often, and even if a conflict arises, dealing with it is easy!

### Working with branches ###

Conceptually speaking, even in the simplest case you are dealing with
two branches, `master` being your local working branch, and
`origin/master` the server copy; you can refer to them by these
names.  However, the concept of branches is much more powerful:

* `git branch newbranch oldbranch` will create a new branch named
  `newbranch` from `oldbranch`, where the latter can refer to a named
  branch such as `master` or to the SHA-1 hash of any commit (you can
  find these checksums via `git log` or `gitg`).

* `git checkout branchname` will switch your working directory to the
  branch `branchname`.  You can now work on this branch independently
  of all others.  (When you push this branch for the first time, git
  will ask you to specify a name for the corresponding upstream
  branch.  Follow the default suggestion!)

* `git merge sourcebranch` will merge  the contents of local branch
  `sourcebranch` into your current working branch.


### More on branches: remote branches ###

*  To work on a new branch that has appeared on the server, but it not
   yet a local to your repository, use

         git fetch origin newbranch
         git checkout newbranch

* If `newbranch` is already known to your local repository, use

         git checkout newbranch
         git pull

* To merge a remote branch _without checking it out as a local
  branch_, type

         git pull origin remotebranch


### Deleting and resetting ###

Git makes branching easy.   You can do experimental work on a
separate branch to later merge or cherry-pick into your main branch.
Sometimes, you'll need to clean up after yourself:

* `git branch -d crazyidea` will delete the branch `crazyidea` in your
  local repository.

* `git push origin :crazyidea` will delete `crazyidea` on the server,
  use with care!  (The command line interface of git is not known for
  its consistency.  The logic behind this seemingly crazy command is
  that it pushes an empty branch into the existing branch `crazyidea`
  in the remote location `origin`.)

* `git reset --hard HEAD` will reset your working copy to the last
  commit, deleting all changes.  Replace HEAD by the SHA-1 of any
  earlier commit if you need to revert more than one commit.


### Getting information from git ###

A list of commands for obtaining status information:

* `git log` will show the commit history of the current branch.

* `git status` shows the branch you are on,  files that
have  changed, and  files that have been staged for commit.

* `git branch` lists all local branches.  Add the `-r`
  switch for remote, or the `-a` switch for all branches
  _known to your local repository_.

* `git remote show origin` lists remote branches _by
querying the server_ and prints information on your local remote
branches.   

* `git config -l` prints global configuration information.


### A note on program-generated output ###

To keep the repository as clean as possible, do not commit output
which is automatically generated by programs such as LaTeX or
Python.  The repository is configured not to add any such
files by default, the exclusion rules are contained in the file
`.gitignore` in the root directory.  If you have figures or other
documents as PDF files from external sources, for example, you should
place them into subdirectories named `figures` or
`literature`.  

In particular, to read any of the `.tex` files in the repository,
you need to run them through LaTeX.


### General advice ###

* Read the output of git commands carefully: the status messages
  usually contain important hints and often good suggestions for
  resolving a problem.

* Do not forget that ["staging" and "committing" are two separate
  steps](https://stackoverflow.com/questions/4878358/why-would-i-want-stage-before-committing-in-git).
  So you have to either use `git commit -am "..."` or do the `git add`
  explicitly.

* Typing `git status` frequently will help you find your way.

* Commit frequently.  Make it a habit to commit before walking away
  from your computer, so you have a commit message explaining where
  you stopped.  (In contrast, push only when you want to share your
  changes.)

### Useful tricks ###

* When you need to change your last commit (because you did not finish
  everything the first time round, or found an error) and wish to keep
  your commit log clean, use

        git commit --amend -m "Commit message"

    (Note: if you have already pushed the changes, you will need to
    use `git push --force` to push the amended commit.  Do this only
    if you are sure that your collaborators have not pulled the old
    version, otherwise you'll introduce a merge conflict for them that
    they'll have to deal with.)

* You sent a copy of your paper to a journal or a colleague and got a
  bunch of comments, referring to specific line numbers and equation
  numbers, in response.  Meanwhile, you have made your own changes, so
  line and equation numbers are now different.  This is a case where
  git shines.  Proceed as follows.

    Find the commit of the version you had previously sent out.  Then

         git branch tmp oldversion
                
    (`oldversion` may be an SHA-1 hash if you did not previously
    branch or tag the version you sent out.)

         git checkout tmp

    (Rerun LaTeX as necessary to get the output files updated as
    well.) Now implement the changes in response to the comments.

         git checkout mybranch
         git merge tmp

    Resolve possible conflicts. (But it works fine without
    intervention surprisingly often!)  Finally, clean up typing

         git branch -d tmp


* For avanced use, [a pull may be separated into a fetch and a merge](http://longair.net/blog/2009/04/16/git-fetch-and-merge/).

