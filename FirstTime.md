## How to install and configure git ##

This checklist describes a few steps that are necessary to be fully up
and running for the two workflows described in this document.

### Install the git program on your computer ###

* _Linux_: Use your distribution software installation tool to install
  the package `git`.  It is strongly recommended to install the
  graphical git browser `gitg` as well (see below).
* _Windows_: Install [Git for
  Windows](https://git-scm.com/download/win).  Important: During
  installation, choose the option "Run Git and included Unix tools
  from the Windows Command Prompt", later "Use OpenSSH".
* _MacOS_: Install [Git for MacOS](https://git-scm.com/download/mac)

### Personal git configuration ###

Open a standard terminal (_Linux_ or _MacOS_) or the git shell
(_Windows_) and type (Insert _your_ name and email address!):

    git config --global user.name "My Name"
    git config --global user.email "xxx@yyy.zzz"
    git config --global color.ui "auto"

Further, git will want to present you a text editor for various
inputs.  If you already have a preferred editor, the choice is clear.
Find the full path to the editor executable and type (modifying
accordingly):

    git config --global core.editor /full/path/to/your/preferred/editor

If you are not using text editors routinely, the following choices are
reasonable defaults:

* _Linux_: The nano editor is a good simple choice, the full path is
  typically `/usr/bin/nano`
* _Windows_: Wordpad is installed by default and suffices
  for the job.
* _MacOS_: _TBA_

It is important that you configure the editor setting in a sensible
way, otherwise you might get stuck later at an inconvenient time.


### Get a bitbucket account ###

The workflows described here need a server that everybody can access.
While it is not difficult to set up a git server within a typical
university infrastructure (all that is required is one machine where
every team member has ssh access), password and permission management
are still a hassle unless the group of collaborators is well defined
and long-time stable.  Using a public cloud service is more convenient
in practice.  Currently, the best choice is
[Bitbucket](https://bitbucket.org/) because this service offers
private repositories without restrictions for academic work.

* [Get a bitbucket account](https://bitbucket.org/account/signup/).
* If you expect to be creating your own repositories (as opposed to
  only collaborating on existing ones), make sure that you [get an
  academic
  account](https://blog.bitbucket.org/2012/08/20/bitbucket-academic/).
* __It is highly recommended__ [to add an ssh key to the
  repository](https://confluence.atlassian.com/bitbucket/add-an-ssh-key-to-an-account-302811853.html).
  Also see [additional information for Windows users](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html).


Note that the workflows described later will not use any bitbucket
specific features, except the somewhat nontrivial access management
when using git for teaching and grading of assignments.

### Graphical git browser (optional) ###

We'll be using git from the command line.  However, it is often useful
to have a graphical tool to visualize project status, history, and
change diffs.  [There are many different graphical git
tools](https://git-scm.com/downloads/guis/) and choice is a matter of
taste.  The following recommendations focus on simplicity and good
visual representation, given that the main interaction with git is via
the command line.

* _Linux_: gitg is a simple repository browser with nice visualization.
* _Windows_: Apparently, gitk is installed by the standard git
  installer for Windows.  However, [gitg is also available for
  Windows](http://ftp.gnome.org/pub/GNOME/binaries/win64/gitg/) and
  has better visuals.
* _MacOS_: [Fork](https://git-fork.com/) appears to be a good tool (_TBC_).


