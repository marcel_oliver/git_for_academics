## The standard work cycle for students ##

As a student, you need to keep track of _two_ repositories on the
server: the master repository where the instructor posts tasks and
possibly lecture notes, and your private branch where you submit your
work and receive comments, grades, and possibly advice or fixes from
the instructor or from the teaching assistant.


### Step 1: Make sure your repository is current ###

Type

    git pull instructor master
    git pull
    
The first command will pull any updates to the class material from the
instructors master repository.  This step assumes that you have
[configured the remote repository `instructor` as described
here](ClassClone.md).  The second command will pull any comments,
grades, or changes that instructor or teaching assistant have done on
your personal branch.  In normal use, there shouldn't be any merge
conflicts.  In the exceptional case that a merge conflict occurs,
follow the suggestions by git to resolve it; [see further comments
here](ShouldKnow.md). 

### Step 2: Do your work and commit it to the repository ###

You change the repository by making a 'commit'.  A commit consists of
a set of changes to the files in your working directory, a commit
message describing your changes, and an SHA-1 hash which is generated
by git as a practically unique identifier.  You may want to group our
work into several commits to keep a more transparent log of what has
been done.  To make a commit, proceed as follows.

1. Do some work (add, modify, or delete files).

2. (Optional): Double-check your work by typing

        git status

    to display which files have changed, or

        git diff [filename]

    for details of changes.

3. Type

        git add filename1 filename2 ...

    to "stage" some files, or `git add .` to  "stage" all files for the commit.

4. Type

        git commit -m "Commit Message"

    to perform the actual commit.  If you do not need to commit new
    files or file deletions, you may "stage" and "commit" with the
    single command

        git commit -am "Commit Message"

### Step 3: Push to the server ###

If you are satisfied with your work and want to submit it, or if you
need to ask for help, push your work to the server as follows.

    git push

Typically, the teaching assistant or instructor will receive an email
from bitbucket that you made a commit.  If you need to get help on a
specific problem, send an email and point the teaching assistant to
your recent version on bitbucket.
