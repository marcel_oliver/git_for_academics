## The standard work cycle ##

For a small-scale scientific collaboration, it is easiest when
everybody works on the same branch.  The name of the default branch is
`master` and we shall assume that all work is done there unless
specified otherwise.  As the repository is distributed, authors can
nevertheless work independently and in parallel without making a mess.
Git will allow for a clean merge of different authors' changes
whenever this is logically possible.  Of course, git cannot prevent
two authors from independently changing the same piece of text or
code, so obviously some minimal coordination is required.  However,
git helps you resolve conflicting changes very transparently, so you
should be much less afraid of such conflicts than you probably are
when you are used to email-based collaborative paper-writing.

### Step 1: Make sure your repository is current ###

Type

    git pull

to download the current state of the server repository to your
computer.


### Step 2: Do your work and commit it to the repository ###

You change the repository by making a 'commit'.  A commit consists of
a set of changes to the files in your working directory, a commit
message describing your changes, and an SHA-1 hash which is generated
by git as a practically unique identifier.  You may want to group our
work into several commits to keep a more transparent log of what has
been done.  To make a commit, proceed as follows.

1. Do some work (add, modify, or delete files).

2. (Optional): Double-check your work by typing

        git status

    to display which files have changed, or

        git diff [filename]

    for details of changes.

3. Type

        git add filename1 filename2 ...

    to "stage" some files, or `git add .` to  "stage" all files for the commit.

4. Type

        git commit -m "Commit Message"

    to perform the actual commit.  If you do not need to commit new
    files or file deletions, you may "stage" and "commit" with the
    single command

        git commit -am "Commit Message"

### Step 3: Push to the server ###

If you are satisfied with your work, send it to the server with

    git push

This concludes the standard work cycle.  Of course, there are a few
more [things you should know](ShouldKnow.md).
