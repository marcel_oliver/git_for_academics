## Collaborate on Overleaf ##

[Overleaf](https://www.overleaf.com/) is an online
[LaTeX](https://www.latex-project.org/) editor which can be included
into a git work flow, albeit with some restrictions. 

### Advantages ###

Using Overleaf has some distinct advantages

* It provides a user interface well suited for LaTeX-novices and
  convenient for many others,

* collaborators on the Overleaf side do not need to know anything
  about `git`,

* You do not need to require collaborators to install any software on
  their computers.

### Disadvantages ###

This convenience comes, quite literally, at a price:

* Access via git (as well as meaningful versioning within Overleaf) is
  only available with a paid Overleaf subscription,

* Commit messages which correspond to Overleaf edits are
  auto-generated and carry no information other than the author (even
  the time stamp is the timestamp when pulling from overleaf, not the
  time stamp of the original edit), vice versa, git commit messages
  are not displayed at the Overleaf end,

* Overleaf supports only one branch (it will not even accept a push to
  a branch different than `master`),

* and the usual drawbacks of any cloud-based service: work on Overleaf
  requires a stable and reasonably fast internet connection, yet there
  are sometimes annoying latencies, and you depend on a service that
  could go away at any time.

### Overleaf _and_ git ###

Using Overleaf together with git allows individual collaborators to
make different decisions regarding these tradeoffs.  Each collaborator
may

* stay fully on the Overleaf side and not know anything about git in
  the background or about possibly co-existing separate project branches,

* work fully on the git side with local tools and full transparency,

* or a combination of the two, e.g. Overleaf for editing LaTeX and
  direct git access for code, merging, and history operations.

The instructions below are only necessary for the project lead and
collaborators who use direct git access.

### Workflow model ###

There are a number of different possible workflow models.  We are
assuming the following setup:

* One or more independent Overleaf projects which each contain the
  entire project.

* A single git repository with a remote copy on Bitbucket, used by at
  least one project lead and possibly other collaborators with their
  own local copies,

* the Overleaf project(s) may be associated with different branches in
  the git repository, but this is not strictly necessary and can be
  decided _ad hoc_.

### Workflow alternatives, not covered here ###

* Overleaf seems to have a way to automatically bridge with GitHub,
  another big git repository hoster.  Due to the restrictions on
  private repositories on GitHub, this would usually mean a second
  paid-subscription provider in the loop.  There is also no clear
  advantage over direct git access, so this model is not pursued
  here.

* It is possible to treat Overleaf projects as
  [submodules]([https://git-scm.com/book/en/v2/Git-Tools-Submodules)
  within a git repository.  This means that the Overleaf user(s) get
  only the restricted view of their submodule.  This may be desirable
  when Overleaf users need to be separated strictly from each other.
  In that case, only the git repository would provide a unified view
  on the entire project.  If strict separation is not required, the
  added complexity of submodules is probably not worth the effort,
  especially since git makes changes transparent so that each Overleaf
  user can make changes throughout the project without creating a
  mess.

### Get started ###

* [Set up Overleaf/git when you are the project lead](OverleafStart.md)
* [Set up git if you are a project member](OverleafMember.md)
* [Alternative instructions from the Overleaf site](https://www.overleaf.com/articles/git-and-overleaf-integration/qmdncpnqwfxx#.WcUSSNOGNE4)

