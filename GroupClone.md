## Clone the repository if you are a project member ##

The first time you are working with a repository, you need to clone it
to your local machine.  In the terminal, navigate to the directory you
wish to use as the parent folder for your local copy of the
repository, then type (using the actual repository address you have
been given):

    git clone ssh://git@bitbucket.org/username/myrepo.git

To enter the repository root directory, type (adapting the name as
necessary):

    cd myrepo

Optional: Put yourself onto the list of watchers for the remote
repository.  You will then get an email every time someone pushes new
work to the server.  To do this, log into Bitbucket, go to the
repository web page on Bitbucket, click on the eye symbol near the top
right corner of the overview page, and subscribe to all notifications.



[You are now ready to start your work](GroupWorkCycle.md).
