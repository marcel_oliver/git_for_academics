## Set up the project with Overleaf/git if you are the project lead ##

The following needs to be done only once, by the project lead.

1. [Set up the repository on bitbucket](SetUp.md) as usual; all
   collaborators who use direct git access must be added on the
   bitbucket side.

2. [Clone the repository to your local computer](GroupClone.md) as usual.

3. Create one or more projects on Overleaf via the [My
   Projects](https://www.overleaf.com/dash) dashboard.  Add the
   Overleaf collaborators.  When collaborators want to do real-time
   collaboration, add them all to a single project.  When their tasks
   are separate (e.g., different authors working on different chapters
   of a book), create one project for each subtask.

4. Connect each of the Overleaf projects to your local repository [as described in this document](https://de.overleaf.com/learn/how-to/How_do_I_push_a_new_project_to_Overleaf_via_git%3F).

5. `git push` the new repository state to bitbucket.

_Note:_ This means that the `origin` points to bitbucket and is the
default push target.  Overleaf is known to git as remote `overleaf`.
If you connect more than one Overleaf project to the repository,
replace `overleaf` by a suitable name for each project in Step 4
above.

